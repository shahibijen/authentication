import {LOG_IN_START,LOG_IN_SUCCESS,LOGOUT,LOG_IN_FAILED,
  SING_UP_SUCCESS,SING_UP_FAILED,SING_UP_START,
  GET_USER_START,GET_USER_SUCCESS,GET_USER_FAILED,
  RESET_PASSWORD_START,RESET_PASSWORD_SUCCESS,RESET_PASSWORD_FAILED
}from '../Types/AuthTypes';

const Login_Start = () => { 
  return {
    type: LOG_IN_START,
  }
}
const Login_Successful = (Token  : string) => {   
  console.log(Token)
    return {
      type: LOG_IN_SUCCESS,
      data : Token 
    }
}
const Login_failed = () => {   
  return {
    type: LOG_IN_FAILED,
  }
}

const LogoutAction = () =>{
  return{
    type : LOGOUT
  }
}

const SingUpStart = () =>{
  return{
    type : SING_UP_START
  }
}
const SingUpSuccess = () =>{
  return{
    type : SING_UP_SUCCESS
  }
}
const SingUpFailed = () =>{
  return{
    type : SING_UP_FAILED
  }
}

const GetProfileStart = () =>{
  return{
    type : GET_USER_START
  }
}
const GetProfileSuccess = (profile : any) => {
  return {
      type : GET_USER_SUCCESS,
      data: profile
  }
}
const GetProfileFailed = () =>{
  return{
    type : GET_USER_FAILED
  }
}

const ResetPasswordStart = () =>{
  return{
    type : RESET_PASSWORD_START
  }
}
const ResetPasswordSuccess = () => {
  return {
      type : RESET_PASSWORD_SUCCESS,

  }
}
const ResetPasswordFailed = () =>{
  return{
    type : RESET_PASSWORD_FAILED
  }
}


export {Login_Start,Login_Successful,LogoutAction,
        Login_failed,SingUpStart,SingUpSuccess,SingUpFailed,
        GetProfileStart,GetProfileSuccess,GetProfileFailed,
        ResetPasswordStart,ResetPasswordSuccess,ResetPasswordFailed
      };